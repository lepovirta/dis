# Docker Images

Helpful Docker images to use in GitLab CI and so on.

The images are hosted in [Gitlab container registry](https://gitlab.com/lepovirta/dis/container_registry).

## Images

### commitlint-gitlab

An image for checking that commits align with conventional commits using [commitlint](https://commitlint.js.org/#/) in GitLab CI pipelines. Example:

```yaml
commitlint:
  stage: setup
  image: registry.gitlab.com/lepovirta/dis/commitlint-gitlab:latest
  script:
  - commitlint-gitlab
  rules:
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```

### crane

[Crane](https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane.md) is used tagging container images built in GitLab CI pipelines. Example:

```yaml
release:
  image: registry.gitlab.com/lepovirta/dis/crane:latest
  stage: release
  before_script:
  - gitlab-auth
  script:
  - crane tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" latest
  rules:
  - if: $CI_COMMIT_BRANCH == "master"
```

### kaniko

[Kaniko](https://github.com/GoogleContainerTools/kaniko) is used for building container images in GitLab CI pipelines. Example:

```yaml
build:
  image: registry.gitlab.com/lepovirta/dis/kaniko:latest
  stage: build
  before_script:
  - gitlab-auth
  script:
  - >
    /kaniko/executor
    --context ${CI_PROJECT_DIR}
    --dockerfile ${CI_PROJECT_DIR}/Dockerfile
    --destination ${CI_REGISTRY_IMAGE}:$CI_COMMIT_SHA
```

### lepo-build

An image containing tools for building and testing static websites using [Hugo](https://gohugo.io/).

### readme-homepage

Generates a homepage from your Markdown README on Gitlab using [Pandoc](https://pandoc.org/).

## License

MIT license

See [LICENSE](LICENSE) for more information.
