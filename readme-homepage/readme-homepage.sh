#!/bin/sh
set -eu

# Resources
RESOURCES_DIR="${RESOURCES_DIR:-"/usr/local/share/readme-homepage"}"
GLOBAL_CSS_PATH="${RESOURCES_DIR}/style.css"
GLOBAL_TEMPLATE_PATH="${RESOURCES_DIR}/template.html"

# Parameters from script args
OUTFILE="public/index.html"
if [ -n "${1:-}" ]; then
    OUTFILE="${1}"
    shift
fi
OUTDIR="$(dirname "${OUTFILE}")"

# Parameters from environment variables
CSS_PATH="${CSS_PATH:-"${OUTDIR}/style.css"}"
TEMPLATE_PATH="${TEMPLATE_PATH:-"${GLOBAL_TEMPLATE_PATH}"}"
TITLE="${TITLE:-"${CI_PROJECT_TITLE}"}"
SOURCE_URL="${SOURCE_URL:-"${CI_PROJECT_URL}"}"
HIGHLIGHT_STYLE=${HIGHLIGHT_STYLE:-"tango"}

# Generate the page
pandoc \
    --template "${TEMPLATE_PATH}" \
    --standalone \
    --from commonmark \
    --output "${OUTFILE}" \
    --css "${CSS_PATH}" \
    --highlight-style "${HIGHLIGHT_STYLE}" \
    --metadata pagetitle="${TITLE}" \
    --metadata sourceurl="${SOURCE_URL}" \
    "$@"

# Copy the CSS next to the generated page
if [ -z "${DISABLE_GENERATE_CSS:-}" ]; then
    cp "${GLOBAL_CSS_PATH}" "${CSS_PATH}"
fi
