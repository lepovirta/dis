#!/bin/sh
set -eu

if [ -z "${CI_MERGE_REQUEST_TARGET_BRANCH_SHA:-}" ]; then
    echo "No target branch SHA found" >&2
    exit 1
fi

if [ -z "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-}" ]; then
    echo "No source branch SHA found" >&2
    exit 1
fi

DEFAULT_CONFIG="/etc/default.commitlint.config.js"
CONFIG_FILES="
commitlint.config.js
.commitlintrc.js
.commitlintrc.json
.commitlintrc.yml
package.json
"

if [ -z "${DISABLE_DEFAULT_COMMITLINT_CONFIG:-}" ]; then
    for config_file in ${CONFIG_FILES}; do
        if [ -s "${config_file}" ]; then
            DISABLE_DEFAULT_COMMITLINT_CONFIG="yes"
            break
        fi
    done
fi

if [ -n "${DISABLE_DEFAULT_COMMITLINT_CONFIG:-}" ]; then
    commitlint \
        --from "${CI_MERGE_REQUEST_TARGET_BRANCH_SHA}" \
        --to "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-}" \
        --verbose
else
    echo "Using default config from ${DEFAULT_CONFIG}" >&2
    commitlint \
        --config "${DEFAULT_CONFIG}" \
        --from "${CI_MERGE_REQUEST_TARGET_BRANCH_SHA}" \
        --to "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-}" \
        --verbose
fi
