#!/busybox/sh
set -eu

mkdir -p /kaniko/.docker
AUTH_B64=$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64)
cat <<EOF > /kaniko/.docker/config.json
{
    "auths": {
        "${CI_REGISTRY}": {
            "auth": "${AUTH_B64}"
        }
    }
}
EOF
